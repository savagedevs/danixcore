# DanixCore

DanixCore is a Rust-based reimplementation of the POSIX core utilities, strictly adhering to POSIX requirements with no additional nonstandard or deprecated features. The primary goal is to provide a set of utilities that are purely POSIX-compliant while ensuring memory safety.

## Installation

Currently, the only way to install DanixCore is by compiling from source using Cargo, Rust's package manager. Once compiled, the output binaries need to be moved to the appropriate locations in the PATH. 

## Usage

DanixCore utilities can be used according to the POSIX standard and their respective man pages. Refer to the man pages for detailed information on each utility's usage and options.

## Contributing

Please refer to the [CONTRIBUTING.md](CONTRIBUTING.md) file for guidelines on contributing to DanixCore.

## License

This project is licensed under the Mozilla Public License 2.0. See the [LICENSE](LICENSE) file for details.

## Additional Information

- For questions or discussions, please open an issue on the GitLab repository.
- Contributions are welcome and appreciated! If you find a bug or have a feature request, feel free to submit a pull request.
- DanixCore is a work in progress. Your feedback and contributions help improve the project and make it more robust.
