/*
echo - write arguments to standard output

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use std::env;

fn main() {
    let args: Vec<String> = env::args().skip(1).collect();

    if args.is_empty() {
        println!();
        return;
    }

    for argument in &args {
        if argument == "-n" { continue; }
        print!("{} ", argument);
    }

    if args[0] != "-n" {
        println!();
    }
}
