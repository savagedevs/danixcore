# Setup on 02/13/2024
- Initialized repository
- Created README.md
- Created CONTRIBUTING.md
- Created LICENSE
- Setup nix development shell

# v0.0.0
- Created Cargo project
- Created echo.rs
- Setup build chain

# v0.0.1
- Basic echo functionally
- Added defualt hedder to CONTRIBUTING.md

# v0.1.0
- Compleated echo
- Added -n option to suppress newline in echo
- Fixed empty arguments bug
