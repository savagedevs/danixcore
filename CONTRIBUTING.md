# Contributing to DanixCore

Thank you for considering contributing to DanixCore! Before submitting your contributions, please take a moment to review the guidelines outlined below.

## Project Overview

DanixCore is a Rust-based reimplementation of the POSIX core utilities, adhering strictly to POSIX requirements while ensuring memory safety. The project aims to provide a purely POSIX-compliant set of utilities with no additional nonstandard features or deprecated functionalities.

## Development Environment

Contributors are encouraged to utilize the provided shell.nix file for consistent development environments.

## Code Style Guidelines

- Variable and function names should follow snake_case convention.
- Use descriptive names to enhance code readability; avoid one-letter variables or poorly named identifiers.
- Code should be self-explanatory; avoid unnecessary comments explaining what the code does.

## Code Formatting

- Code formatting should adhere to the Rust style guide, with one exception: tabs should be used instead of spaces.
- All source files should use the following header with the right information filled in:
```
/*
name - one line

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 First Last

Maintainer(s): First Last <your@email.tld>
*/
```

## Testing Guidelines

- While the compiler's safety tests are essential, functional testing should also be performed manually to ensure compatibility with POSIX standards.

## Documentation

- Documentation should be provided in the form of man pages for each completed POSIX utility.
- Rustdoc is not used to avoid having to make in code redundant comments; focus on creating comprehensive man pages.

## Contributing Process and Review

1. Fork the repository on GitLab.
2. Push your changes to the dev branch in your fork.
3. Submit a merge request to the dev branch of the main repository.
4. Contributions will be reviewed for adherence to POSIX conventions and compilation on the maintainers' machines.
5. Approved changes will be merged into the dev branch for further testing.

## Licensing

All contributions to DanixCore are subject to the terms of the Mozilla Public License (MPL). Please review the LICENSE.md file for details.

## Guidelines

Contributors are expected to adhere to the following guidelines:

- Don't be a jerk.
- Write high-quality code; no excuses.
- Accept constructive criticism from maintainers regarding code quality.
- Talk only of the code; avoid engaging in politics or identity-related issues.

By contributing to DanixCore, you agree to abide by these guidelines and contribute positively to the project's growth and success.

Thank you for your interest in contributing to DanixCore!
